﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstExercise.Validation.Validators
{
    class IntConvertibleValidator : IValidator<string>
    {
        public void Validate(string val)
        {
            if (!int.TryParse(val, out int x))
            {
                throw new ValidationException($"Cannot convert {val} to int.");
            }
        }
    }

}
