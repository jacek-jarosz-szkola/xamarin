﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Threading;

namespace EssentialsTest
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private static void ReVibrate(object data)
        {
            Vibration.Vibrate();
        }

        private Timer vibrationTimer = new Timer(ReVibrate, new object(), Timeout.Infinite, 500);


        


        private async void Button_On_Clicked(object sender, EventArgs e)
        {        
            await Flashlight.TurnOnAsync();

            vibrationTimer.Change(0, 500);
        }

        private async void Button_Off_Clicked(object sender, EventArgs e)
        {

            await Flashlight.TurnOffAsync();

            vibrationTimer.Change(Timeout.Infinite, 500);
        }
    }
}
