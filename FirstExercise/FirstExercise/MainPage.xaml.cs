﻿using FirstExercise.Validation;
using FirstExercise.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Xamarin.Forms;

namespace FirstExercise
{
    public partial class MainPage : ContentPage
    {

        //private ValidationManager FormValidationManager;

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        private string _age;
        public string Age
        {
            get => _age;
            set
            {
                _age = value;
                OnPropertyChanged();
            }
        }

    


        private string _result;
        public string Result
        {
            get => _result;
            set
            {
                _result = value;
                OnPropertyChanged();
            }
        }

        private string _adultMessage;
        public string AdultMessage
        {
            get => _adultMessage;
            set
            {
                _adultMessage = value;
                OnPropertyChanged();
            }
        }

        private Color _color;
        public Color Color
        {
            get => _color;
            set
            {
                _color = value;
                OnPropertyChanged();
            }
        }

        private ICommand _checkResultCommand;
        public ICommand CheckResultCommand {
            get
            {
                if (_checkResultCommand == null)
                {
                    _checkResultCommand = new Command(() => {
                        CalculateResult(Name, Age, out string res, out string adultRes, out bool isError);
                        Result = res;
                        Color = isError ? Color.Red : Color.Default;
                        AdultMessage = adultRes;
                    });
                }
                return _checkResultCommand;
            } 
        }


        public MainPage()
        {
            InitializeComponent();
        }

        private void checkButton_Clicked(object sender, EventArgs e)
        {
            CalculateResult(entryName.Text, entryAge.Text, out string result, out string adultMsg, out bool isError);
            labelResult.Text = result;
            labelAdultMessage.Text = adultMsg;
            labelResult.TextColor = isError ? Color.Red : Color.Default;
        }

        
       
        private void CalculateResult(string name, string ageStr, out string result, out string adultMsg, out bool isError)
        {
            isError = false;
            adultMsg = "";
            try
            {
                //                FormValidation.ValidateForm(name, ageStr);
                ValidationManager validationManager = new ValidationManager
                {
                    Validations = new Dictionary<string, IValidation>()
                    {
                        {
                            "Imię" ,
                            new Validation<string>(name)
                            {
                                Validators = new List<IValidator<string>>()
                                {
                                 new StringNotNullOrWhitespaceValidator(),
                                 new StringRegexValidator(new Regex("^[a-zA-Z ]+$"))
                                }
                            }
                        },
                        {
                            "Wiek",
                            new Validation<string>(ageStr)
                                {
                                Validators = new List<IValidator<string>>()
                                {
                                    new StringNotNullOrWhitespaceValidator(),
                                    new IntConvertibleValidator(),
                                    new StringIntInRangeValidator(0, 200)
                                }
                            }
                        }

                    }
                };

                validationManager.Execute();

                int age = int.Parse(ageStr);
                adultMsg = (age >= 18) ? "Pełnoletni" : "Niepełnoletni";
                result = $"Witaj, {name}";
            }
            catch (ValidationException e)
            {
                result = e.Message;
                isError = true;
            }
        } 

        /*
        private bool ValidateNameNotEmpty(string name, out string message)
        {
            
            if(string.IsNullOrWhiteSpace(name))
            {
                message = "Imie nie może być puste";
                return false;
            }
            message = "";
            return true;
        }

        private bool ValidateAgeConvertible(string ageStr, out string message)
        {
            if (!int.TryParse(ageStr, out int age))
            {
                message = "Nie można przekonwertować wieku.";
                return false;
            }
            message = "";
            return true;
        }

        private bool ValidateAgeInRange(int age, out string message)
        {
            if (age < 0 || age > 200)
            {
                message = "Wiek poza skalą";
                return false;
            }
            message = "";
            return true;
        }

        private void CalculateResult(string name, string ageStr, out string result, out string adultMsg)
        {
            adultMsg = "";
            if (!ValidateNameNotEmpty(name, out result)) return;

            if (!ValidateAgeConvertible(ageStr, out result)) return;

            int age = int.Parse(ageStr);

            if (!ValidateAgeInRange(age, out result)) return;

            adultMsg = (age >= 18) ? "Pełnoletni" : "Niepełnoletni";
            result = $"Witaj, {name}";
        }

*/
    }
}





















































































































































//hejo jaca