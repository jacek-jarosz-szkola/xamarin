﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstExercise.Validation
{
    class ValidationException : Exception
    {
        public ValidationException(string msg) : base(msg) { }
        public ValidationException(string msg, ValidationException inner) : base(msg + " : " + inner.Message, inner) { }

    }
}
