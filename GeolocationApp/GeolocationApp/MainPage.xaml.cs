﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Threading;
using System.Runtime.CompilerServices;

namespace GeolocationApp
{



    public partial class MainPage : ContentPage
    {


        public Location Target { get; set; }

        public Location Home { get; set; } = new Location(50.911773968766425, 20.722570023827664);

        private double _distance;

        public double Distance { 
            get => _distance;
            set { 
                _distance = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DistanceStr));
            } 
        }

        public string DistanceStr => Math.Round(_distance, 2).ToString() + "km"; 

        public MainPage()
        {
            InitializeComponent();
            var updateTask = Task.Run(UpdateLoop);
        }


        public async void UpdateLoop()
        {
            while (true)
            {
                UpdateLocation();
                await Task.Delay(1000);
            }
        }

        

        private async void UpdateLocation()
        {
            try
            {
                Target = await Geolocation.GetLastKnownLocationAsync();
                double distanceKm = Location.CalculateDistance(Home, Target, DistanceUnits.Kilometers);
                Distance = distanceKm;
            } catch (PermissionException e)
            {
                return;
            }
        }
    }
}
