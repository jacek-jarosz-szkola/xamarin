﻿using FirstPracticalTask.View;
using FirstPracticalTask.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FirstPracticalTask
{
    public partial class MainPage : ContentPage
    {
        

        public PhotoInfoViewModel ViewModel { get; set; }

        private CarouselImagesPage carouselImagesPage;
        public MainPage()
        {
            ViewModel = new PhotoInfoViewModel();
            carouselImagesPage = new CarouselImagesPage(ViewModel);
            InitializeComponent();
            BindingContext = ViewModel;

            
        }

        private void buttonShowImagesCarousel_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(carouselImagesPage);
        }

        private void buttonShowImagesList_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ImagesListPage(ViewModel));
        }

        private void buttonAddPhoto_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddPhotoPage(ViewModel));
        }

        //private void buttonBindingPage_Clicked(object sender, EventArgs e)
        //{
        //    Navigation.PushAsync(new BindingPage());
        //}
    }   
}       
        