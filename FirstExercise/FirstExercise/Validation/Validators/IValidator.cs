﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstExercise.Validation.Validators
{
    interface IValidator<T>
    {
        void Validate(T val);

    }
}
