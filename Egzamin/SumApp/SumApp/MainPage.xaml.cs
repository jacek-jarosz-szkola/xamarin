﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SumApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private int _minuty;
        private int _sekundy;

        private void ButtonDolicz_Clicked(object sender, EventArgs e)
        {
            int inMinuty;
            int inSekundy;

            if (!int.TryParse(EntryMinutes.Text, out inMinuty) ||
                !int.TryParse(EntrySeconds.Text, out inSekundy) ||
                inMinuty < 0 ||
                inSekundy < 0
                )
            {
                DisplayAlert("Błąd", "Podano niepoprawne dane", "ok");
                return;
            }



            _minuty += inMinuty;
            _sekundy += inSekundy;

            if (_sekundy >= 60)
            {
                _minuty += _sekundy / 60;
                _sekundy = _sekundy % 60;
            }


            LabelLacznyCzas.Text = MakeTimeString();
        }

        private string MakeTimeString()
        {
            string sString = _sekundy < 10 ? $"0{_sekundy}" : $"{_sekundy}";

            return $"{_minuty}:{sString}";
        }



        private void ButtonResetuj_Clicked(object sender, EventArgs e)
        {
            _minuty = 0;
            _sekundy = 0;
            EntryMinutes.Text = "";
            EntrySeconds.Text = "";
            LabelLacznyCzas.Text = MakeTimeString();
        }
    }
}
