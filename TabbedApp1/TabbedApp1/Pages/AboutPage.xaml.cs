﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedApp1.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        private double gradOffset = 0.0;
        public double GradientOffset {
            get => gradOffset;
            set
            {
                gradOffset = value;
                OnPropertyChanged(nameof(GradientOffset));
            }
        }




        public AboutPage()
        {
            InitializeComponent();
        }

        private void GradientAnimator()
        {
            while (gradOffset < 0.5)
            {
                GradientOffset += 0.01;
                Thread.Sleep(100);
            }
            GradientOffset = 0.0;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Task.Run(GradientAnimator);
        }
    }
}