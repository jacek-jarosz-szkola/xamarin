﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Internals;

namespace FirstExercise.Validation
{
    class ValidationManager
    {
        private Dictionary<string, IValidation> validations;

        public Dictionary<string, IValidation> Validations
        {
            get => validations;
            set => validations = value;
        }

        public ValidationManager()
        {

        }

        public void Execute()
        {
            foreach( KeyValuePair<string, IValidation> row in validations)
            {
                try
                {
                    row.Value.Execute();
                } catch (ValidationException e)
                {
                    throw new ValidationException($"Error in field {row.Key}, {e.Message}");
                }
            }
                
        }
    }
}
