﻿using FirstPracticalTask.ViewModel;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace FirstPracticalTask.Commands
{
    public class TestCommand : ICommand
    {
        private PhotoInfoViewModel viewModel;
        public TestCommand(PhotoInfoViewModel viewModel)
        {
            this.viewModel = viewModel;
        }


        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return parameter is TestCommandParams;
        }

        public void Execute(object parameter) {
            if (parameter is TestCommandParams testCommandParams) {
                viewModel.AddPhoto(testCommandParams.Name, "https://static.wikia.nocookie.net/memes/images/9/9a/Troll-face.jpg/revision/latest?cb=20150924183516&path-prefix=pl");
            }
        }
        
    }
}

