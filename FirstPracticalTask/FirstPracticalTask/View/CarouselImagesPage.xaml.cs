﻿using FirstPracticalTask.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FirstPracticalTask.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CarouselImagesPage : CarouselPage
    {
        public PhotoInfoViewModel ViewModel { get; set; }

        public CarouselImagesPage(PhotoInfoViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            BindingContext = ViewModel;
        }
    }
}