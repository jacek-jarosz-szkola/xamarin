﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstPracticalTask.Model
{
    public class PhotoInfo
    {
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
    }
}
