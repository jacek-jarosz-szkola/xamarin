﻿using CarouselApp.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace CarouselApp.ViewModel
{
    public class PhotoInfoViewModel
    {
        public ObservableCollection<PhotoInfo> Photos { get; set; }
        public ICommand RemovePhotoCommand { get; set; }

        public PhotoInfoViewModel()
        {
            Photos = new ObservableCollection<PhotoInfo>();
            Photos.Add(new PhotoInfo
            {
                Name = "Pies",
                Url = "https://www.karo.waw.pl/media/products/017fee9990cef7edbb2a4436b4ebd1d8/images/thumbnail/large_kocyk-z-mikroflaneli-120x150-piesek-pies-slodki-pieseczek-jack-russel-terrier-dog-pled-dzieciecy-4935.jpg?lm=1652438458"
            });
            Photos.Add(new PhotoInfo
            {
                Name = "Kot",
                Url = "https://www.megapedia.pl/wp-content/uploads/2017/10/kot-syjamski-6282881-1024x683.jpg"
            });
            Photos.Add(new PhotoInfo
            {
                Name = "Szop",
                Url = "https://www.pestworld.org/media/561871/istock_000001190722_large.jpg"
            });

            RemovePhotoCommand = new Command(param =>
            {
                if (param is string photoName)
                {
                    Photos.Remove(Photos.FirstOrDefault(photo => photo.Name == photoName));
                }
            });

        }
    }
}
