﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstExercise.Validation.Validators
{
    class StringIntInRangeValidator : IValidator<string>
    {
        private int min;
        private int max;
        public StringIntInRangeValidator(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        public void Validate(string val)
        {
            if(int.TryParse(val, out int number))
            {
                if(number < min || number > max)
                {
                    throw new ValidationException($"Number: {val} not in range <{min}, {max}>");
                }
            }
        }
    }
}
