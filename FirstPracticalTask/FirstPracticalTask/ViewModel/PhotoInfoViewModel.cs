﻿using FirstPracticalTask.Model;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FirstPracticalTask.ViewModel
{
    public class PhotoInfoViewModel : INotifyPropertyChanged
    {
        public string Title { get; set; }

        public ObservableCollection<PhotoInfo> ListOfPhoto { get; set; }
        
        public void AddPhoto(string name, string url)
        {
            ListOfPhoto.Add(new PhotoInfo { Name = name, PhotoUrl = url });
        }
        public void RemovePhoto(PhotoInfo photo)
        {
            ListOfPhoto.Remove(photo);
        }
        public PhotoInfoViewModel()
        {
            Title = "Lista zdjęć";
            ListOfPhoto = new ObservableCollection<PhotoInfo>
            {
                new PhotoInfo { Name = "Pies", PhotoUrl = "https://www.pieski.net/psy/d/rudo-plochacz-lezacy-pies-bialy-holenderski.jpeg"},
                new PhotoInfo { Name = "Kot", PhotoUrl = "https://tapety.tja.pl/obrazki/tja_normalne/234040.jpg"},
                new PhotoInfo { Name = "Szop", PhotoUrl = "https://images4.alphacoders.com/650/thumb-1920-650174.jpg"},
            };

        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
