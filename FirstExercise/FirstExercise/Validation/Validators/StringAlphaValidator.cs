﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstExercise.Validation.Validators
{
    class StringAlphaValidator : IValidator<string>
    {
        public void Validate(string val)
        {
            if (!val.All(x => char.IsLetter(x))) 
            {
                throw new ValidationException($"{val} doesn't consist from only letters.");
            }
        }
    }
}
