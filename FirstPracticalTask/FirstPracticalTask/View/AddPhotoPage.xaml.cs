﻿using FirstPracticalTask.ViewModel;
using System;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FirstPracticalTask.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddPhotoPage : ContentPage
    {
        public string PhotoName { get; set; }
        public string PhotoUrl { get; set; }

        public PhotoInfoViewModel ViewModel { get; set; }

        public AddPhotoPage(PhotoInfoViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            
        }

        private void buttonAddPhoto_Clicked(object sender, EventArgs e)
        {
            ViewModel.AddPhoto(PhotoName, PhotoUrl);
            Navigation.PopAsync();
        }
    }
}