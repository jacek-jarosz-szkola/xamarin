﻿using FirstExercise.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstExercise.Validation
{
    class Validation<T> : IValidation
    {
        private List<IValidator<T>> validators;
        public List<IValidator<T>> Validators
        {
            get => validators;
            set => validators = value;
        }

        T val;

        public T Value
        {
            get => val;
            set => val = value;
        }

        public void AddValidator(IValidator<T> validator)
        {
            validators.Add(validator);
        }

        public Validation()
        {

        }


        public Validation(T value)
        {
            val = value;
        }

        public Validation(T value, List<IValidator<T>> validatorList)
        {
            val = value;
            validators = validatorList;
        }

        public void Execute()
        {
            try
            {
                validators.ForEach(x => x.Validate(val));
            }
            catch (ValidationException e)
            {
                throw new ValidationException($"Error in field {name}", e);
            }

        }

        
    }
}
