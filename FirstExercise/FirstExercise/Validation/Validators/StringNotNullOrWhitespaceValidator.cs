﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstExercise.Validation.Validators
{
    class StringNotNullOrWhitespaceValidator : IValidator<string>
    {
        public void Validate(string val)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                throw new ValidationException("String is null or contains only whitespaces.");
            }
        }
    }
}
