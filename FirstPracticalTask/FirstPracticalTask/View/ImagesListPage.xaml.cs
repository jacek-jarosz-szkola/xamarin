﻿using FirstPracticalTask.Model;
using FirstPracticalTask.ViewModel;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FirstPracticalTask.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImagesListPage : ContentPage
    {
        public PhotoInfoViewModel ViewModel { get; set; }

        public PhotoInfo SelectedPhoto { get; set; }

        public ImagesListPage(PhotoInfoViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
            BindingContext = ViewModel;
        }

        private void buttonReturn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void buttonRemove_Clicked(object sender, EventArgs e)
        {
            ViewModel.RemovePhoto(SelectedPhoto);
        }
    }
}