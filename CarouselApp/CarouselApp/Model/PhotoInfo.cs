﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace CarouselApp.Model
{
    public class PhotoInfo
    {
        public string Name { get; set; }
        public string Url { get; set; }
        
    }
}
