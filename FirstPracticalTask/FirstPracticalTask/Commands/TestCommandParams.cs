﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstPracticalTask.Commands
{
    public struct TestCommandParams
    {
        public string Name { get; set; }
    }
}
