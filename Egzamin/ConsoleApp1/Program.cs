﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {

        /**
         * ZnajdzNajlepszyCzas
         * znajduje najlepszy czas na określone kilometry
         * Parametry wejściowe:
         * int[] czasy - tablica czasów dla każdego kilometra trasy
         * int wielkoscOdcinka - wielkość odcinka (w km), którego czas jest porównywany
         * Wartość zwracana:
         * int - najlepszy czas odcinka w kilometrach podanych przez parametr
       **/
        static int ZnajdzNajlepszyCzas(int[] czasy, int wielkoscOdcinka)
        {
            int najlepszyCzas = int.MaxValue;


            for (int i = 0; i < czasy.Length - wielkoscOdcinka + 1; i += 1)
            {
                int czasTeraz = 0;
                for (int j = 0; j < wielkoscOdcinka; j++)
                {
                    czasTeraz += czasy[i + j];
                }
                if (czasTeraz < najlepszyCzas) najlepszyCzas = czasTeraz;
            }
            return najlepszyCzas;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Podaj ile kilometrów przebiegłeś:");
            int kilometry;
            if (!int.TryParse(Console.ReadLine(), out kilometry) || kilometry < 0)
            {
                Console.WriteLine("Podano niepoprawną daną");
                return;
            }

            int[] czasy = new int[kilometry];

            Console.WriteLine("Podaj czasy (w sekundach) na każdym kilometrze:");

            for (int i = 0; i < kilometry; i++)
            {
                Console.Write($"Kilometr {i + 1}:");
                int czas;
                if (!int.TryParse(Console.ReadLine(), out czas) || czas < 0)
                {
                    Console.WriteLine("Podano niepoprawną daną");
                    return;
                }

             
                czasy[i] = czas;

            }

            int wielkoscOdcinka;
            Console.Write("Podaj jakiej wielkości najlepszego odcinka szukasz (w km):");
            if (!int.TryParse(Console.ReadLine(), out wielkoscOdcinka) || wielkoscOdcinka < 0)
            {
                Console.WriteLine("Podano niepoprawną daną");
                return;
            }

            int najlepszyCzas = ZnajdzNajlepszyCzas(czasy, wielkoscOdcinka);

            Console.WriteLine($"Najlepszy czas na {wielkoscOdcinka}km {najlepszyCzas}s");

        }
    }
}
