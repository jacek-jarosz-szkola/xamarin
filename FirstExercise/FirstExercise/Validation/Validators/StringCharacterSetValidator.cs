﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstExercise.Validation.Validators
{
    class StringCharacterSetValidator : IValidator<string>
    {
        List<char> charSet;

        public StringCharacterSetValidator(List<char> allowedChars)
        {
            charSet = allowedChars;
        }
        public void Validate(string val)
        {
            if (!val.All(x => charSet.Contains(x)))
            {
                throw new ValidationException($"{val} contains disallowed characters.");
            }
        }
    }
}
