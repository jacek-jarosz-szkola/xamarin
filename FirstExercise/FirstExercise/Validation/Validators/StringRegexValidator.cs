﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FirstExercise.Validation.Validators
{
    class StringRegexValidator : IValidator<string>
    {
        private Regex regex;
        public StringRegexValidator(Regex regex)
        {
            this.regex = regex;
        }

        public void Validate(string val)
        {
            if(!regex.IsMatch(val))
            {
                throw new ValidationException($"String {val} doesnt match regular expression {regex}.");
            }
        }
    }
}
